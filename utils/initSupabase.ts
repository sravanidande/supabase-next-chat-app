import { createClient } from '@supabase/supabase-js'
import {
  NEXT_PUBLIC_SUPABASE_API_KEY,
  NEXT_PUBLIC_SUPABASE_URL,
} from './envLocals'

export const supabase = createClient(
  NEXT_PUBLIC_SUPABASE_URL,
  NEXT_PUBLIC_SUPABASE_API_KEY,
)
