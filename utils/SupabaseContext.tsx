import type { Session, SupabaseClient } from '@supabase/supabase-js'
import React from 'react'
import { supabase } from './initSupabase'

export interface User {
  readonly id: string
  readonly username: string
}

interface SupabseContext {
  readonly session: Session | null
  readonly supabase: SupabaseClient
  readonly currentUser: User | undefined
}

const SupabaseContext = React.createContext<SupabseContext | undefined>(
  undefined,
)

export const useSupabase = () => {
  const ctx = React.useContext(SupabaseContext)

  if (ctx === undefined) {
    throw new Error('use SupabseProvider')
  }
  return ctx
}

export const SupabseProvider: React.FC = ({ children }) => {
  const [currentUser, setCurrentUser] = React.useState<User | undefined>(
    undefined,
  )
  const [session, setSession] = React.useState(() => supabase.auth.session())

  supabase.auth.onAuthStateChange((_event, session) => {
    setSession(session)
  })

  React.useEffect(() => {
    const getCurrentUser = async () => {
      if (session?.user?.id) {
        const { data } = await supabase
          .from('user')
          .select('*')
          .eq('id', session.user.id)

        if (data?.length) {
          const foundUser = data[0] as User
          setCurrentUser(foundUser)

          const str = `user:id=eq.${foundUser.id}`
          supabase
            .from(str)
            .on('UPDATE', payload => {
              setCurrentUser(payload.new)
            })
            .subscribe()
        }
      }
    }

    getCurrentUser().catch(err => console.error(err))
  }, [session])

  return (
    <SupabaseContext.Provider value={{ session, supabase, currentUser }}>
      {children}
    </SupabaseContext.Provider>
  )
}
