/* eslint-disable react/jsx-no-bind */
import { Button, Flex, Heading } from '@chakra-ui/react'
import React from 'react'
import { useSupabase } from '../utils'

export const Signin = () => {
  const { supabase } = useSupabase()

  const [error, setError] = React.useState<string | undefined>()

  const handleSigninGithub = () => {
    supabase.auth
      .signIn({ provider: 'github' })
      .then(() => setError(undefined))
      .catch((err: Error) => {
        console.error(err)
        setError(err.message)
      })
  }

  return (
    <>
      <h2 style={{ color: 'red' }}>{error}</h2>
      <Flex h="100vh" justify="center" align="center" direction="column">
        <Heading>Supabase Chat</Heading>
        <Button onClick={handleSigninGithub} mt="8">
          {error ? 'Try sign in again?' : 'Sign in with GitHub'}
        </Button>
      </Flex>
    </>
  )
}
