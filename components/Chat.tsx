/* eslint-disable @typescript-eslint/await-thenable */
/* eslint-disable no-return-await */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable @typescript-eslint/naming-convention */
import {
  Box,
  Button,
  Flex,
  Heading,
  HStack,
  Input,
  StackDivider,
  Text,
  VStack,
} from '@chakra-ui/react'
import React from 'react'
import { useImmer } from 'use-immer'
import { User, useSupabase } from '../utils'
import { EditUserNameForm } from './EditForm'

interface Message {
  readonly id: number
  readonly content: string
  readonly created_at: string
  readonly user_id: string
}

interface MessageState {
  readonly messages: readonly Message[]
  readonly text: string
  readonly isEdit: boolean
  readonly users: Map<User['id'], User>
}

const initialState: MessageState = {
  messages: [],
  isEdit: false,
  text: '',
  users: new Map(),
}

export const ChatPage = () => {
  const { supabase, session, currentUser } = useSupabase()

  const [{ isEdit, messages, text, users }, update] = useImmer(initialState)

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = async evt => {
    evt.preventDefault()

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    await supabase
      .from('message')
      .insert([{ user_id: session?.user?.id, content: text }])

    update(draft => {
      draft.text = ''
    })
  }

  React.useEffect(() => {
    const getMessages = async () => {
      const { data: messages } = await supabase.from('message').select('*')

      if (!messages) {
        return
      } else {
        update(draft => {
          draft.messages = messages as Message[]
        })
      }
    }
    getMessages().catch(err => {
      console.log(err)
    })

    const setupMessageSubscription = async () => {
      await supabase
        .from('message')
        .on('INSERT', ({ new: newMessage }) =>
          update(draft => {
            draft.messages.push(newMessage)
          }),
        )
        .subscribe()
    }
    setupMessageSubscription().catch(err => console.log(err))
  }, [])

  React.useEffect(() => {
    const getUsers = async () => {
      const { data: newUsers } = await supabase.from('user').select('*')

      if (newUsers?.length === 0) {
        return
      } else {
        const users = new Map<User['id'], User>()
        for (const user of newUsers as readonly User[]) {
          users.set(user.id, user)
        }
        update(draft => {
          draft.users = users
        })

        supabase
          .from('user')
          .on('*', payload => {
            if (
              payload.eventType === 'INSERT' ||
              payload.eventType === 'UPDATE'
            ) {
              // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
              update(draft => {
                draft.users.set((payload.new as User).id, payload.new)
              })
            }
          })
          .subscribe()
      }
    }
    getUsers().catch(err => console.log(err))
  }, [])

  const handleSignout: React.MouseEventHandler<HTMLButtonElement> = evt => {
    evt.preventDefault()
    window.localStorage.clear()
    window.location.reload()
  }

  return (
    <Flex direction="column" position="relative" h="100vh">
      <Box bg="lightgrey">
        <Flex h="80px" p="4px" alignItems="center">
          <Heading flex="1" justifyContent="center">
            Supabase Chat
          </Heading>
          <HStack>
            <>
              {currentUser?.username
                ? currentUser?.username
                : session?.user?.email}
            </>
            {isEdit ? (
              <EditUserNameForm
                onEdit={() =>
                  update(draft => {
                    draft.isEdit = false
                  })
                }
              />
            ) : (
              <Button
                onClick={() =>
                  update(draft => {
                    draft.isEdit = !isEdit
                  })
                }
              >
                Edit
              </Button>
            )}
            <Button onClick={handleSignout}>sign out</Button>
          </HStack>
        </Flex>
      </Box>

      <VStack
        divider={<StackDivider borderColor="gray.200" />}
        spacing={4}
        align="stretch"
        p="8px"
      >
        {messages.map((msg, idx) => (
          <Box h="30px" key={idx}>
            <span>
              {users.get(msg.user_id)?.username === null
                ? users.get(msg.user_id)?.id
                : users.get(msg.user_id)?.username}
            </span>
            <Text>{msg.content}</Text>
          </Box>
        ))}
      </VStack>

      <Box w="100%" p="4" position="absolute" bottom="0" bg="grey">
        <form onSubmit={handleSubmit}>
          <HStack>
            <Input
              placeholder="write message"
              required
              value={text}
              variant="filled"
              size="lg"
              onChange={evt =>
                update(draft => {
                  draft.text = evt.target.value
                })
              }
              bg="lightgrey"
            />
            <Button type="submit">Send message</Button>
          </HStack>
        </form>
      </Box>
    </Flex>
  )
}
