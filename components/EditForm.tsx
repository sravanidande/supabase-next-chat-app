/* eslint-disable @typescript-eslint/await-thenable */
/* eslint-disable react/jsx-no-bind */
import { Button, HStack, Input } from '@chakra-ui/react'
import React from 'react'
import { useSupabase } from '../utils'

interface EditUserNameProps {
  onEdit(): void
}

export const EditUserNameForm: React.FC<EditUserNameProps> = ({ onEdit }) => {
  const { supabase, currentUser } = useSupabase()
  const [editUser, setEditUser] = React.useState(currentUser?.username)

  const handleSubmit = async () => {
    await supabase.from('user').upsert([{ ...currentUser, username: editUser }])
    onEdit()
  }

  return (
    <form onSubmit={handleSubmit}>
      <HStack>
        <Input
          placeholder="write message"
          // required
          value={editUser || ''}
          variant="filled"
          size="lg"
          onChange={evt => setEditUser(evt.target.value)}
          bg="lightgrey"
        />
        <Button type="submit">Update</Button>
      </HStack>
    </form>
  )
}
