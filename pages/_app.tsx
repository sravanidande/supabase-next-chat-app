import { ChakraProvider } from '@chakra-ui/react'
import { enableMapSet } from 'immer'
import type { AppProps } from 'next/app'
import React from 'react'
import { SupabseProvider } from '../utils'

enableMapSet()

export default ({ Component, pageProps }: AppProps) => (
  <ChakraProvider>
    <SupabseProvider>
      <Component {...pageProps} />
    </SupabseProvider>
  </ChakraProvider>
)
