import React from 'react'
import { Signin } from '../components/Signin'
import { ChatPage } from '../components/Chat'
import { useSupabase } from '../utils'

export const HomePage = () => {
  const { currentUser } = useSupabase()

  return <>{currentUser ? <ChatPage /> : <Signin />}</>
}
